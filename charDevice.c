#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *,size_t, loff_t *);


#define SUCCESS 0 
#define DEVICE_NAME "charDevice"
#define BUF_LEN 80

static int Major;
static int Device_Open = 0;

static char msg[BUF_LEN];
static char *msg_Ptr;

static struct file_operations fops = { .read = device_read, .open = device_open, .release = device_release};

int init_module(void){
   Major = register_chrdev(0, DEVICE_NAME, &fops);

   if(Major < 0){
       printk(KERN_ALERT "No se puede registrar el dispositivo char con %d\n", Major);
       return Major;
   }

   printk(KERN_INFO "charDevice: Nico tu driver fue registrado con exito. \n");

   return SUCCESS;
}

void cleanup_module(void){
     unregister_chrdev(Major, DEVICE_NAME);
     printk(KERN_INFO "charDevice: Nico tu driver fue desregistrado\n");
}

static int device_open(struct inode *inode, struct file *file){
    static int counter = 0;
    if(Device_Open)
        return -EBUSY;

    Device_Open++;
    sprintf(msg, "Ya te dije %d veces Hola Mundo!\n",counter ++);
    msg_Ptr = msg;
    try_module_get(THIS_MODULE);

    return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file){
    Device_Open--;
    module_put(THIS_MODULE);
    
    return 0;
}

static ssize_t device_read(struct file *flip, char *buffer, size_t length, loff_t * offset){
    int bytes_read = 0;
    if(*msg_Ptr == 0){
        return 0;
    }

    while(length && *msg_Ptr){
        put_user(*(msg_Ptr++), buffer++);
        length--;
        bytes_read++;
    }

    return bytes_read;
}



MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nico");
MODULE_DESCRIPTION("Un primer charDev");